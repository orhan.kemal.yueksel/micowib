const Path = require('path');

module.exports = {
  plugins: [
    require('postcss-import'),
    require('tailwindcss')(Path.resolve(__dirname, '../tailwindcss/tailwind.js')),
    require('autoprefixer'),
  ],
};
