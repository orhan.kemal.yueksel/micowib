'use strict';

const Path = require('path');

const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {

  devServer: {
    contentBase: Path.resolve(__dirname, '../../target/example-application'),
  },

  entry: Path.resolve(__dirname, '../../source/example-application/webpack-entry.jsx'),

  module: {
    rules: [

      {
        test: /\.p?css$/,
        include: Path.resolve(__dirname, '../../source'),
        use: [
          { loader: 'style-loader' },
          {
            loader: 'css-loader',
            options: { importLoaders: 1 }
          },
          {
            loader: 'postcss-loader',
            options: {
              config: {
                path: Path.resolve(__dirname, '../../config/postcss')
              },
            }
          }
        ]
      },

      {
        test: /\.jsx?$/,
        include: Path.resolve(__dirname, '../../source'),
        use: [{
          loader: 'babel-loader',
          options: {
            presets: [[
              '@babel/preset-react',
            ]],
          }
        }],
      },
    ],
  },

  mode: 'development',

  output: {
    filename: 'webpack-output.js',
    path: Path.resolve(__dirname, '../../target/example-application'),
    publicPath: '',
  },

  plugins: [
    new CopyWebpackPlugin([{
      test: /\.html$/,
      from: Path.resolve(__dirname, '../../source'),
      to: Path.resolve(__dirname, '../../target'),
    }]),
  ],

  resolve: {
    extensions: [ '*', '.js', '.jsx' ],
  },
};
