'use strict';

const Path = require('path');

module.exports = {
  entry: Path.resolve(__dirname, '../../source/example-application/webpack-entry.js'),
  module: {
    rules: [
      {
        test: /\.js$/,
        include: Path.resolve(__dirname, '../../source'),
        use: [{
          loader: 'babel-loader',
          options: {
            presets: [[
              '@babel/preset-env',
              {
                targets: {
                  browsers: '> 1%',
                },
              },
            ]],
          }
        }],
      },
    ],
  },
  output: {
    filename: 'webpack-output.js',
    path: Path.resolve(__dirname, '../../target'),
    publicPath: '',
  },
  resolve: {
    extensions: [ '*', '.js', '.vue', '.json' ],
  },
};
