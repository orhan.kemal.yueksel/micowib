import React, { useState } from 'react';

import ArchiveContent from './ArchiveContent';

export default function Archive(props) {
  return (
    <div className={props.className}>
      <h1 className="text-pink">Archive</h1>
      <ul>
        <li>Info</li>
        <li onClick={
          e => props.micowibNext(
            ArchiveContent,
            { title: props.micowibValue.title + "-Content" }
          )
        }>
          Content
        </li>
        <li>Graph</li>
      </ul>
    </div>
  );
};
