import React, { useState } from 'react';

import Workspace from './Workspace';

export default function Root(props) {
  // dummy workspaces:
  const [workspaces, setWorkspaces] = useState([
    { title: 'Workspace-A' },
    { title: 'Workspace-B' },
    { title: 'Workspace-C' },
  ]);

  const [newWorkspaceName, setNewWorkspaceName] = useState('');

  return (
    <div className={props.className}>
      <h1 className="text-pink">
        Archipel
      </h1>
      <ul>
        <li>Enter a workspace:</li>
        <ul>
          { workspaces.map(
            workspace =>
              <li
                key={workspace.title}
                onClick={(e) => props.micowibNext(Workspace, workspace)}
              >
                {workspace.title}
              </li>
          )}
        </ul>
        <li>
          <input
            onChange={e => setNewWorkspaceName(e.target.value)}
            onKeyPress={e => {
              if (e.key === 'Enter') {
                setWorkspaces(workspaces.concat({ title: newWorkspaceName }));
                setNewWorkspaceName('');
              }}}
            placeholder="Create new workspace"
            type="text"
            value={newWorkspaceName}
          />
        </li>
      </ul>
    </div>
  );
};
