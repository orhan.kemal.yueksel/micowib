import React, { useState } from 'react';

import Archive from './Archive';

export default function Workspace(props) {

  // dummy archives:
  const archives = ["A","B","C"].map(letter => ({
    title: props.micowibValue.title + "-Archive-" + letter
  }));

  return (
    <div className={props.className}>
      <h1 className="text-pink">Workspace</h1>
      <ul>
        <li>Enter an archive:</li>
        <ul>
          {
            archives.map(
              archive =>
                (<li
                key={archive.title}
                onClick={(e) => props.micowibNext(Archive, archive)}
           >
             {archive.title}
           </li>)
            )
          }
        </ul>
        <li>New archives:</li>
        <ul>
          <li>Create new archive</li>
          <li>Clone archive</li>
        </ul>
        <li>Workspace:</li>
        <ul>
          <li>Delete this workspace</li>
          <li>Rename this workspace</li>
        </ul>
      </ul>
    </div>
  );
};
