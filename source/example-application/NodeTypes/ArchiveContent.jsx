import React, { useState } from 'react';

export default function ArchiveContent(props) {

  // dummy contents:
  const contents = ["A","B","C"].map(letter => ({
    title: props.micowibValue.title + '-' + letter
  }));

  return (
    <div className={props.className}>
      <h1 className="text-pink">Content</h1>
      <ul>
        {
          contents.map(
            content => (
              <li
                key={content.title}
                onClick={e => props.micowibNext(ArchiveContent, content)}
              >
                {content.title}
              </li>)
          )
        }
      </ul>
    </div>
  );
};
