import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';

import './styles.css';

import Root from './NodeTypes/Root';
import Workspace from './NodeTypes/Workspace';

ReactDOM.render(
  <App/>,
  document.getElementById('react-mount-point')
);

function App() {

  const maxNumOfColumns = 0; // 0 means there's no maximum

  // set default workspace:
  const [columns, setColumns] = useState([
    { type: Root, value: { title: '' } },
  ]);

  useEffect(() => {
    // scroll breadcrumbs and miller-columns to the right.
    for (let parent of ['#breadcrumbs', '#miller-columns']) {
      let rightest = document.querySelector(parent + ' > :last-child');
      if (rightest) rightest.scrollIntoView();
    }
  });

  function micowibNext(type, value, index){
    setColumns(
      columns
        .slice(0, index + 1)
        .concat([{
          type: type,
          value: value
        }]));
  }

  return (
    <div className="h-screen w-full flex flex-col">

      {/* miller columns */}
      <div
        id="miller-columns"
        className="h-full flex flex-row min-w-full overflow-x-auto"
      >

        { columns.map(
          (column, index) =>
            React.createElement(
              column.type,
              {
                className: "p-6" + (
                  (index + 1 !== columns.length)
                  ? " min-w-1/3 border-r border-pink"
                    : " min-w-2/3"),
                key: index,
                micowibNext: (type, value) => micowibNext(type, value, index),
                micowibValue: column.value,
              }
            )
        ).slice(-maxNumOfColumns) // TODO: efficiency
        }

      </div>

      {/* breadcrumbs */}
      <ul
        id="breadcrumbs"
        className="bg-pink flex italic inline-block list-reset overflow-x-auto text-lg text-white w-full"
      >

        {/* loop over columns */}

        { columns.map(
          (column, index) =>
            <React.Fragment>

              {/* breadcrumb */}
              <li
                key={index}
                className="py-4 px-2 hover:bg-pink-dark cursor-pointer whitespace-no-wrap"
                onClick={e => setColumns(columns.slice(0, index + 1))}
              >
                {column.value.title}
              </li>

              {/* arrow */}
              { (index + 1 !== columns.length) && // no arrow behind last breadcrumb
                <li
                  key={index+'-arrow'}
                  className="py-4 px-2 whitespace-no-wrap"
                >
                  ›
                </li>
              }
            </React.Fragment>

          /* skip first column */
        ).slice(1) // TODO: efficiency
        }
      </ul>

    </div>
  );
}
